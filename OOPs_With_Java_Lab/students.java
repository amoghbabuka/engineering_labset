interface Student
{
    void Display_Grade();
    void Display_Atten();
}
class PG_Student implements Student
{
    String name, grade,usn;
    int m1, m2, m3, attendence, total;
    PG_Student(String name, String usn, int m1, int m2, int m3, int attendence)
    {
        this.name = name;
        this.usn = usn;
        this.m1 = m1;
        this.m2 = m2;
        this.m3 = m3;
        this.attendence = attendence;
    }
    void Display()  
    {
        System.out.println("The Name is " + name+ "the usn is" +usn+"\n");
        System.out.println("Marks are " + m1 + " " + m2 + " " + m3);
    }
    public void Display_Atten()
    {
        System.out.println("The attendence is " + attendence);
    }
    public void Display_Grade()
    {
        total = (m1 + m2 + m3)/3;
        if (total < 40){
            grade = "Fail";
        }
        else if (total >=40 && total <= 60){
            grade = "Pass";
        } 
        else if(total > 75){
            grade = "Distinction";
        }
        else{
            grade ="Will Update shortly";
        }
        System.out.println("The Grade is " + grade);
    }
}
class UG_Student implements Student
{
    String name,usn, grade;
    int m1, m2, m3, attendence, total;
    UG_Student(String name,String usn, int m1, int m2, int m3, int attendence)
    {
        this.name = name;
        this.usn =usn;
        this.m1 = m1;
        this.m2 = m2;
        this.m3 = m3;
        this.attendence = attendence;
    }
    void Display()
    {
        System.out.println("Name is " + name+"the usn is"+usn+"\n");
        System.out.println("Marks are " + m1 + " " + m2 + " " + m3);
    }
    public void Display_Atten()
    {
        System.out.println("The attendence is " + attendence);
    }
    public void Display_Grade()
    {
        total = (m1 + m2 + m3)/3;
        if (total < 40){
            grade = "Fail";
        }
        else if (total >=40 && total <= 60){
            grade = "First Class";
        } 
        else if(total > 60){
            grade = "Distinction";
        }
        else{
            grade ="Will Update shortly";
        }
        System.out.println("The Grade is " + grade);
    }
}
class StudentDemo {
    public static void main(String[] args) {
        PG_Student pg = new PG_Student("Alex","4PS13CS056", 45, 68, 47, 35);
        pg.Display();
        pg.Display_Atten();
        pg.Display_Grade();
        UG_Student ug = new UG_Student("Aman","4PS14CS056", 95, 88, 77, 25);
        ug.Display();
        ug.Display_Atten();
        ug.Display_Grade();
    }
}