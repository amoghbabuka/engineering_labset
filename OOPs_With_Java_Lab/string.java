import java.util.Scanner;
public class string
{
    public static void main (String[] args)
    {
        Scanner ip = new Scanner(System.in);
        System.out.println("Enter Your Name:");
        String str = ip.nextLine();
        System.out.println("The Length of the given string is : "+str.length());
        System.out.print("\nEnter the Character: ");
        char ch = ip.next().charAt(0);
        if(str.indexOf(ch) != -1) {
            System.out.println("The string contains the character "+ch);
        } 
        else {
            System.out.println("The string does not contain the character "+ch);
        }
        int frequency = 0;
        for (int i=0; i<str.length();i++)
        {
            if(str.charAt(i) == ch)
            {
                frequency = frequency+1;
            }
        }
        System.out.println("Frequency of "+ch+" is "+frequency);
        System.out.print("\nThe character "+ch+" is present at indices - ");
        int index = str.indexOf(ch);
        while (index >= 0)
        {
            System.out.print(index+" ");
            index = str.indexOf(ch, index + 1);
        }
    }
}