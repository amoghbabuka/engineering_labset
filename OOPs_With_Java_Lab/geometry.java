import java.util.Scanner;

class area_calculate {
    void area(float x) {
        System.out.println("Area of circle is "+(3.14*x*x));
    }
    void area(float x, float y) {
        System.out.println("Area of Rectangle is "+(x*y));
    }
    void area(float x, float y, float z) {
        if(x==2) {
            float area = (float) (0.5*z*y);
            System.out.println("Area of Triangle is "+area);
        } 
    }
}
public class geometry {
    public static void main(String args[]) {
        Scanner ip = new Scanner(System.in);
        boolean yes;
        area_calculate c = new area_calculate();
        do{
        System.out.println("1 - Circle");
        System.out.println("2 - Triangle");
        System.out.println("3 - Rectangle");
        System.out.print("Enter your choice : ");
        int x = ip.nextInt();
        switch(x) {
            case 1: System.out.println("******************Area of Circle***********************");
					System.out.println("Enter the radius : ");
                    float r = ip.nextFloat();
                    c.area(r);
                    break;
            case 2: System.out.println("******************Area of Triangle***********************");
					System.out.println("Enter the length of a side : ");
                    float tl = ip.nextFloat();
                    System.out.println("Enter the breath of a side : ");
                    float tb = ip.nextFloat();
                    c.area(2, tb, tl);
                    break;
            case 3: System.out.println("******************Area of Rectangle***********************");
					System.out.println("Enter the Breath : ");
                    float l = ip.nextFloat();
                    System.out.println("Enter the Height : ");
                    float b = ip.nextFloat();
                    c.area(l, b);
                    break;
            default:System.out.println("Please select in above three options, thank you!"); 
					System.exit(0);
                    break;         
        }
        System.out.print("Would you like to continue? (y/n): ");
        String check = ip.next();
        if (check.equalsIgnoreCase("y"))
        {
            yes = true;
        }

        else
        {
            yes = false;
        }

    }
    while(yes == true);
    System.out.print("*****Thank You*******");
    }
}
