/* Program:
Write a Java Program to handle the following exceptions based on the choice made by the user by writing
suitable try and catch block.
    - Arithmetic Exception
    - ArrayIndexOutOfException
    - StringIndexOutOfBoundException
    - NullPointerException
*/
import java.util.Scanner;
public class exceptions{
    public static void main (String[] args)
    {
        Scanner sc = new Scanner(System.in); 
        boolean inp=true;
        //While loop is used for the program repetation
        while(inp)
        {
            System.out.println("Choose the Options mentioned below\n 1.Arithmetic Exception\n 2.ArrayIndexOutOfException\n 3.StringIndexOutOfBoundException\n 4.NullPointerException\n Enter Your Choice:");
            //Obtaining the User Input
            int option = sc.nextInt();
            switch(option){
                case 1: System.out.println("\nPerforming the Arithmetic Exception\n");
                        try { 
                            System.out.println("Enter One Integers:\n");
                            int a = sc.nextInt();         
                            System.out.println ("Result = " + (a/0));  // cannot divide by zero Error 
                        } 
                        catch(ArithmeticException e) { 
                            System.out.println ("It throws divide by zero error which means unable to divide the value by zero."); 
                        }
                break;
                case 2: System.out.println("\nPerforming the ArrayIndexOutOfException\n");
                        try{ 
                            int a[] = new int[5];           //The Size of the Array a is 5
                            a[6] = 9;                       //Tring to access 7th element in an array a(i.e. 6th index Value)
                        } 
                        catch(ArrayIndexOutOfBoundsException e){ 
                            System.out.println ("It throws an Error as Array Index out of Bound Exception, Where I am not able to Access the 6th Element of Array."); 
                        }
                break;
                case 3: System.out.println("\nPerforming the StringIndexOutOfBoundException\n");
                        try { 
                        String a = "This is like chipping";       //The length of string a is 22 
                        char c = a.charAt(24); // Trying to access the 25th element of string a (i.e. 24th index value) 
                        System.out.println(c); 
                        } 
                        catch(StringIndexOutOfBoundsException e) { 
                            System.out.println("It throws an Error as string Index out of Bound Exception, Where It is not able to Access the 24th Element of string"); 
                        } 
                break;
                case 4: System.out.println("\nPerforming the NullPointerException\n");
                        try { 
                            String name = null;                      //Assigning a null value 
                            System.out.println(name.charAt(0)); 
                        } catch(NullPointerException e) { 
                            System.out.println("It throws an error called Null Pointer Exception, where the name variable has null value in it."); 
                        } 
                break;
                default: System.out.println("\nThe Entered Choise is Invalid, Unable to proceed further.\n");
            }
            System.out.println("\nDo you want to Continue the Process, if Yes give as Y or else N");
            char inp1 = sc.next().charAt(0);
            if (inp1 == 'Y' || inp1 == 'y')
                inp = true;
            else
                inp = false;
        }
    }
}